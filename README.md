# Atelier 3 : Droits et Merge Requests


## Modifier les droits du projet, afin que :
- [ ] Personne ne soit autorisé à faire un push sur la branche principale du projet, à savoir main
- [ ] Seul le mainteneur du projet, puisse faire les merge

## Faire un fork d’un projet d’un autre assistant de l’Information
- [ ] Faire quelques commits sur ce Fork

## Créer une Merge Request vers la branche main du projet initial
- [ ] Sur votre projet initial, regarder si quelqu’un a posté une Merge Request.
- [ ] Si c’est le cas, accepter la.


## Commit 1

## Commit 2

## Commit 3

